package com.report.newsreporter.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.report.newsreporter.R;
import com.report.newsreporter.data.pojo.Reporter;

import java.util.List;

public class ReporterlistAdapter extends RecyclerView.Adapter<ReporterlistAdapter.Reporterholder> {


    Context context;
    List<Reporter>reporters;

    public ReporterlistAdapter(Context context, List<Reporter> reporters) {
        this.context = context;
        this.reporters = reporters;
    }

    public class Reporterholder extends RecyclerView.ViewHolder{
        TextView txtdatemessage;
        ImageButton imgbtnCall;

        public Reporterholder(@NonNull View itemView) {
            super(itemView);
            txtdatemessage=itemView.findViewById(R.id.txtdatemessage);
            imgbtnCall=itemView.findViewById(R.id.imgbtnCall);
            imgbtnCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+reporters.get(getAdapterPosition()).getDsMobile()));
                    context.startActivity(intent);

                }
            });
        }
    }

    @NonNull
    @Override
    public Reporterholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_reportadapter,parent,false);
        return new Reporterholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Reporterholder holder, int position) {

        holder.txtdatemessage.setText(reporters.get(position).getDsReporterUser()+"\n"+reporters.get(position).getDsMobile());

    }

    @Override
    public int getItemCount() {
        return reporters.size();
    }
}
