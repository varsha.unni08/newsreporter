package com.report.newsreporter.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.newsreporter.R;
import com.report.newsreporter.activities.MainActivity;
import com.report.newsreporter.activities.NewsReportDetailsActivity;
import com.report.newsreporter.data.pojo.NewsReport;

import java.util.List;

public class NewsReportAdapter extends RecyclerView.Adapter<NewsReportAdapter.NewHolder> {

    Context context;
    List<NewsReport>newsReports;

    public NewsReportAdapter(Context context, List<NewsReport> newsReports) {
        this.context = context;
        this.newsReports = newsReports;
    }

    public class NewHolder extends RecyclerView.ViewHolder{

        ImageView imgNews;
        TextView txtTitle,txtViews;

        public NewHolder(@NonNull View itemView) {
            super(itemView);
            imgNews=itemView.findViewById(R.id.imgNews);
            txtTitle=itemView.findViewById(R.id.txtTitle);
            txtViews=itemView.findViewById(R.id.txtViews);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent=new Intent(context, NewsReportDetailsActivity.class);
                    intent.putExtra("newsreport",newsReports.get(getAdapterPosition()));
                    context.startActivity(intent);
                }
            });

        }
    }

    @NonNull
    @Override
    public NewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_newsreportadapter,parent,false);

        return new NewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewHolder holder, int position) {

        if(newsReports.get(position)!=null) {

            if(newsReports.get(position).getDsImage()!=null) {

                if (!newsReports.get(position).getDsImage().equals("")) {

                    Glide.with(context)
                            .load(newsReports.get(position).getDsImage())

                            .placeholder(R.drawable.picture)
                            .into(holder.imgNews);


                }
            }
            if(newsReports.get(position).getDsNewsHead()!=null) {
                holder.txtTitle.setText(newsReports.get(position).getDsNewsHead()+"\n"+newsReports.get(position).getDt_record());
            }

            if(newsReports.get(position).getTotalReviews()!=null) {

                if (!newsReports.get(position).getTotalReviews().equals("")) {

                    holder.txtViews.setText(newsReports.get(position).getTotalReviews() + " Reviews");
                }
            }
        }

    }

    @Override
    public int getItemCount() {
        return newsReports.size();
    }
}
