package com.report.newsreporter.webservice;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import com.report.newsreporter.data.pojo.District;
import com.report.newsreporter.data.pojo.Logindata;
import com.report.newsreporter.data.pojo.NewsCategory;
import com.report.newsreporter.data.pojo.NewsPreview;
import com.report.newsreporter.data.pojo.NewsPreviewResponse;
import com.report.newsreporter.data.pojo.NewsReport;
import com.report.newsreporter.data.pojo.OtpGenerateData;
import com.report.newsreporter.data.pojo.PinCode;
import com.report.newsreporter.data.pojo.RegisterData;
import com.report.newsreporter.data.pojo.Reporter;
import com.report.newsreporter.data.pojo.ReporterProfile;
import com.report.newsreporter.data.pojo.State;
import com.report.newsreporter.data.pojo.Village;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface WebInterfaceimpl {

    @FormUrlEncoded
    @POST("reporterUser.php?apicall=login")
    Call<Logindata> getLoginData(@Field("phone") String user, @Field("password") String password);


    @Multipart
    @POST("reporter.php?apicall=signup")
    Call<List<RegisterData>> getRegistration(@Part("ds_reporter_user")RequestBody requestBody, @Part("ds_pincode")RequestBody ds_pincode, @Part("ds_mobile")RequestBody ds_mobile
                                   , @Part("ds_education_qual")RequestBody ds_education_qual, @Part MultipartBody.Part multipartBody, @Part("ds_aadhaar")RequestBody ds_aadhaar
    , @Part MultipartBody.Part adhar, @Part("ds_email")RequestBody ds_email, @Part("dt_birth")RequestBody dt_birth, @Part("ds_gender")RequestBody ds_gender, @Part("password")RequestBody password, @Part MultipartBody.Part ds_employee_image, @Part MultipartBody.Part police_verification
    , @Part("ds_transaction")RequestBody ds_transaction);



    @GET("reporterOtpGenerator.php")
    Call<List<OtpGenerateData>>generateOTp();

    @FormUrlEncoded
    @POST("reporterOtpVerification.php")
    Call<List<OtpGenerateData>>otpVerification(@Field("otp")String otp);

    @GET("reporterProfile.php")
    Call<List<ReporterProfile>>getReporterProfile();

    @Multipart
    @POST("reporterEditProfileImage.php")
    Call<List<RegisterData>>EditProfileimg(@Part MultipartBody.Part ds_employee_image);

    @GET("stateDistPin_reporter.php?apicall=state")
    Call<List<State>>getState();

    @GET("stateDistPin_reporter.php?apicall=district")
    Call<List<District>>getDistrict(@Query("state")String state);

    @GET("stateDistPin_reporter.php?apicall=pincode")
    Call<List<PinCode>>getPincode(@Query("district")String district);

    @GET("stateDistPin_reporter.php?apicall=village")
    Call<List<Village>>getVillage(@Query("pincode")String pincode);


    @POST("reporter_newses.php")
    Call<List<NewsReport>>getReporterNews();

    @POST("reporter_news_categories.php")
    Call<List<NewsCategory>>getNewsCategory();

    @FormUrlEncoded
    @POST("search_news_bydate.php")
    Call<List<NewsReport>>getSearchNewsByDate(@Field("dt_record")String dt_record);


    @Multipart
    @POST("news.php")
    Call<List<NewsPreviewResponse>>postNews(@Part("ds_news_head")RequestBody ds_news_head, @Part("ds_news_content")RequestBody ds_news_content,
                                            @Part("ds_news_priority")RequestBody ds_news_priority, @Part("cd_news_category")RequestBody cd_news_category,
                                            @Part MultipartBody.Part ds_image, @Part MultipartBody.Part ds_video);



    @FormUrlEncoded
    @POST("reporter_details.php")
    Call<List<Reporter>>getReporterdetail(@Field("pincode")String pincode);



















//    @GET("adm_patterns.php")
//    Call<List<Pattern>>getPatters();
//
//    @GET("adm_product_stock.php")
//    Call<List<StockData>>getProductStock(@Query("cd_pattern") String cd_pattern);
//
//    @GET("adm_product_stock.php")
//    Call<List<Branchdata>>getBranchData(@Query("cd_product") String cd_product);
}
