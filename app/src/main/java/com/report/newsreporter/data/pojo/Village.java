package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Village {

    @SerializedName("cd_village")
    @Expose
    private String cdVillage;
    @SerializedName("ds_village")
    @Expose
    private String dsVillage;

    public Village() {
    }

    public String getCdVillage() {
        return cdVillage;
    }

    public void setCdVillage(String cdVillage) {
        this.cdVillage = cdVillage;
    }

    public String getDsVillage() {
        return dsVillage;
    }

    public void setDsVillage(String dsVillage) {
        this.dsVillage = dsVillage;
    }
}
