package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewsCategory implements Serializable {

    @SerializedName("cd_news_category")
    @Expose
    private String cdNewsCategory;
    @SerializedName("ds_news_category")
    @Expose
    private String dsNewsCategory;

    public NewsCategory() {
    }

    public String getCdNewsCategory() {
        return cdNewsCategory;
    }

    public void setCdNewsCategory(String cdNewsCategory) {
        this.cdNewsCategory = cdNewsCategory;
    }

    public String getDsNewsCategory() {
        return dsNewsCategory;
    }

    public void setDsNewsCategory(String dsNewsCategory) {
        this.dsNewsCategory = dsNewsCategory;
    }
}
