package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewsReport implements Serializable {

    @SerializedName("cd_news")
    @Expose
    private String cdNews;
    @SerializedName("ds_news_head")
    @Expose
    private String dsNewsHead;
    @SerializedName("ds_news_content")
    @Expose
    private String dsNewsContent;
    @SerializedName("ds_image")
    @Expose
    private String dsImage;
    @SerializedName("ds_video")
    @Expose
    private String dsVideo;
    @SerializedName("newsRating")
    @Expose
    private Object newsRating;
    @SerializedName("totalReviews")
    @Expose
    private String totalReviews;
    @SerializedName("dt_record")
    @Expose
    private String dt_record;

    public NewsReport() {
    }

    public String getDt_record() {
        return dt_record;
    }

    public void setDt_record(String dt_record) {
        this.dt_record = dt_record;
    }

    public String getCdNews() {
        return cdNews;
    }

    public void setCdNews(String cdNews) {
        this.cdNews = cdNews;
    }

    public String getDsNewsHead() {
        return dsNewsHead;
    }

    public void setDsNewsHead(String dsNewsHead) {
        this.dsNewsHead = dsNewsHead;
    }

    public String getDsNewsContent() {
        return dsNewsContent;
    }

    public void setDsNewsContent(String dsNewsContent) {
        this.dsNewsContent = dsNewsContent;
    }

    public String getDsImage() {
        return dsImage;
    }

    public void setDsImage(String dsImage) {
        this.dsImage = dsImage;
    }

    public String getDsVideo() {
        return dsVideo;
    }

    public void setDsVideo(String dsVideo) {
        this.dsVideo = dsVideo;
    }

    public Object getNewsRating() {
        return newsRating;
    }

    public void setNewsRating(Object newsRating) {
        this.newsRating = newsRating;
    }

    public String getTotalReviews() {
        return totalReviews;
    }

    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }
}
