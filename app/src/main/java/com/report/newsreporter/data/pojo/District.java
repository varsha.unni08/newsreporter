package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class District {

    @SerializedName("cd_district")
    @Expose
    private String cdDistrict;
    @SerializedName("cd_state")
    @Expose
    private String cdState;
    @SerializedName("ds_district")
    @Expose
    private String dsDistrict;

    public District() {
    }

    public String getCdDistrict() {
        return cdDistrict;
    }

    public void setCdDistrict(String cdDistrict) {
        this.cdDistrict = cdDistrict;
    }

    public String getCdState() {
        return cdState;
    }

    public void setCdState(String cdState) {
        this.cdState = cdState;
    }

    public String getDsDistrict() {
        return dsDistrict;
    }

    public void setDsDistrict(String dsDistrict) {
        this.dsDistrict = dsDistrict;
    }
}
