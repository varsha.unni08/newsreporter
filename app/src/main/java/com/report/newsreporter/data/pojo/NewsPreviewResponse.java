package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsPreviewResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cd_news")
    @Expose
    private String cdNews;

    public NewsPreviewResponse() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCdNews() {
        return cdNews;
    }

    public void setCdNews(String cdNews) {
        this.cdNews = cdNews;
    }
}
