package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PinCode {

    @SerializedName("cd_pincode")
    @Expose
    private String cdPincode;
    @SerializedName("cd_state")
    @Expose
    private String cdState;
    @SerializedName("cd_district")
    @Expose
    private String cdDistrict;
    @SerializedName("ds_pincode")
    @Expose
    private String dsPincode;

    public PinCode() {
    }

    public String getCdPincode() {
        return cdPincode;
    }

    public void setCdPincode(String cdPincode) {
        this.cdPincode = cdPincode;
    }

    public String getCdState() {
        return cdState;
    }

    public void setCdState(String cdState) {
        this.cdState = cdState;
    }

    public String getCdDistrict() {
        return cdDistrict;
    }

    public void setCdDistrict(String cdDistrict) {
        this.cdDistrict = cdDistrict;
    }

    public String getDsPincode() {
        return dsPincode;
    }

    public void setDsPincode(String dsPincode) {
        this.dsPincode = dsPincode;
    }
}
