package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReporterProfile implements Serializable {
    @SerializedName("ds_reporter_user")
    @Expose
    private String dsReporterUser;
    @SerializedName("ds_mobile")
    @Expose
    private String dsMobile;
    @SerializedName("ds_email")
    @Expose
    private String dsEmail;
    @SerializedName("ds_employee_image")
    @Expose
    private String dsEmployeeImage;
    @SerializedName("dt_birth")
    @Expose
    private String dtBirth;

    public ReporterProfile() {
    }

    public String getDsReporterUser() {
        return dsReporterUser;
    }

    public void setDsReporterUser(String dsReporterUser) {
        this.dsReporterUser = dsReporterUser;
    }

    public String getDsMobile() {
        return dsMobile;
    }

    public void setDsMobile(String dsMobile) {
        this.dsMobile = dsMobile;
    }

    public String getDsEmail() {
        return dsEmail;
    }

    public void setDsEmail(String dsEmail) {
        this.dsEmail = dsEmail;
    }

    public String getDsEmployeeImage() {
        return dsEmployeeImage;
    }

    public void setDsEmployeeImage(String dsEmployeeImage) {
        this.dsEmployeeImage = dsEmployeeImage;
    }

    public String getDtBirth() {
        return dtBirth;
    }

    public void setDtBirth(String dtBirth) {
        this.dtBirth = dtBirth;
    }
}
