package com.report.newsreporter.data.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reporter {

    @SerializedName("cd_reporter_user")
    @Expose
    private String cdReporterUser;
    @SerializedName("ds_reporter_user")
    @Expose
    private String dsReporterUser;
    @SerializedName("ds_mobile")
    @Expose
    private String dsMobile;

    public Reporter() {
    }

    public String getCdReporterUser() {
        return cdReporterUser;
    }

    public void setCdReporterUser(String cdReporterUser) {
        this.cdReporterUser = cdReporterUser;
    }

    public String getDsReporterUser() {
        return dsReporterUser;
    }

    public void setDsReporterUser(String dsReporterUser) {
        this.dsReporterUser = dsReporterUser;
    }

    public String getDsMobile() {
        return dsMobile;
    }

    public void setDsMobile(String dsMobile) {
        this.dsMobile = dsMobile;
    }
}
