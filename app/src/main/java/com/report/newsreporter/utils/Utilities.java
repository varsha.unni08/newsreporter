package com.report.newsreporter.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.webkit.MimeTypeMap;

import androidx.core.content.ContextCompat;

import java.io.File;

public class Utilities {

    public static boolean checkPermission(Context context,String permission)
    {
        boolean a=false;

        if(ContextCompat.checkSelfPermission(context,permission)!= PackageManager.PERMISSION_GRANTED)
        {
            a=false;
        }
        else {

            a=true;
        }

        return a;


    }

    public static String getMimeType(File file)
    {
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        int index = file.getName().lastIndexOf('.')+1;
        String ext = file.getName().substring(index).toLowerCase();
        String type = mime.getMimeTypeFromExtension(ext);

        return type;
    }
}
