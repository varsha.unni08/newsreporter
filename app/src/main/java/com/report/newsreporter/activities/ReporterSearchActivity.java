package com.report.newsreporter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.report.newsreporter.R;
import com.report.newsreporter.adapters.ReporterlistAdapter;
import com.report.newsreporter.data.pojo.Reporter;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReporterSearchActivity extends AppCompatActivity {

    ImageView imgback;
    ProgressFragment progressFragment;
    Button btnSearch;
    EditText edtPin;
    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporter_search);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        edtPin=findViewById(R.id.edtPin);
        btnSearch=findViewById(R.id.btnSearch);
        recycler=findViewById(R.id.recycler);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtPin.getText().toString().equals(""))
                {

                    callReporterSearch(edtPin.getText().toString());

                }
                else {

                    Toast.makeText(ReporterSearchActivity.this,"Enter pin code",Toast.LENGTH_SHORT).show();
                }



            }
        });






    }


    public void callReporterSearch(String pin)
    {


        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfkdj");


        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(ReporterSearchActivity.this).create(WebInterfaceimpl.class);

        Call<List<Reporter>>jsonArrayCall=webInterfaceimpl.getReporterdetail(pin);
        jsonArrayCall.enqueue(new Callback<List<Reporter>>() {
            @Override
            public void onResponse(Call<List<Reporter>> call, Response<List<Reporter>> response) {
                progressFragment.dismiss();
                if(response.body()!=null)
                {

                    if(response.body().size()>0)
                    {

                        recycler.setLayoutManager(new LinearLayoutManager(ReporterSearchActivity.this));
                        recycler.setAdapter(new ReporterlistAdapter(ReporterSearchActivity.this,response.body()));




                    }

                    else {

                        Toast.makeText(ReporterSearchActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                    }





                }
                else {

                    Toast.makeText(ReporterSearchActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<List<Reporter>> call, Throwable t) {
                progressFragment.dismiss();

            }
        });


    }
}
