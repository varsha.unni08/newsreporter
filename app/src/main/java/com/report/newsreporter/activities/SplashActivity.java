package com.report.newsreporter.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.VideoView;

import com.report.newsreporter.R;
import com.report.newsreporter.data.appconstants.Literals;
import com.report.newsreporter.preferencehelper.PreferenceHelper;

public class SplashActivity extends AppCompatActivity {

    Thread thread;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();


//        String path = "android.resource://" + getPackageName() + "/" + R.raw.testlive;
//        videoView.setVideoURI(Uri.parse(path));
//        videoView.start();
        thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try{

                    thread.sleep(4000);
                }catch (Exception e)
                {

                }

                if(new PreferenceHelper(SplashActivity.this).getBoolData(Literals.isRegistered)) {

                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
                else {

                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        });
        thread.start();


    }


}
