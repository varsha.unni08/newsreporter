package com.report.newsreporter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.report.newsreporter.R;

public class TermsConditionsActivity extends AppCompatActivity {

    AppCompatImageView imgback;

    Button btn;
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);
        getSupportActionBar().hide();
        btn=findViewById(R.id.btn);
        webview=findViewById(R.id.webview);
        imgback=findViewById(R.id.imgback);

       String htmlcontent="<html><body style=\"text-align:justify\">Terms and Conditions agreements act as a legal contract between you (the company) who has the website or mobile app and the user who access your website and mobile app.\n" +
               "        Having a Terms and Conditions agreement is completely optional. No laws require you to have one. Not even the super-strict and wide-reaching General Data Protection Regulation (GDPR).</body></Html>";

       webview.setVerticalScrollBarEnabled(true);
       webview.loadData(htmlcontent,"text/html","utf-8");

       imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TermsConditionsActivity.this,RegisterActivity.class));

                finish();

            }
        });
    }
}
