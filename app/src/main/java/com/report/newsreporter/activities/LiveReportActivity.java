package com.report.newsreporter.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.report.newsreporter.R;
import com.report.newsreporter.utils.Utilities;

import java.io.File;
import java.net.URI;

public class LiveReportActivity extends AppCompatActivity {

    TextView txtDob;

    ImageView imgrecord;
    AppCompatImageView imgback;

    Button btnrecord;

    static final int REQUEST_VIDEO_CAPTURE = 1;

    Uri videoUri=null;
    VideoView videoview;
    LinearLayout layoutVideorecordbtn;
    Handler handler;
    EditText edtTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_report);
        getSupportActionBar().hide();
        handler=new Handler();

        txtDob=findViewById(R.id.txtDob);
        imgrecord=findViewById(R.id.imgrecord);
        imgback=findViewById(R.id.imgback);
        btnrecord=findViewById(R.id.btnrecord);
        videoview=findViewById(R.id.videoview);
        layoutVideorecordbtn=findViewById(R.id.layoutVideorecordbtn);
        edtTitle=findViewById(R.id.edtTitle);


        btnrecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edtTitle.getText().toString().equals("")) {

                    if (Utilities.checkPermission(LiveReportActivity.this, Manifest.permission.CAMERA)) {
                        dispatchTakeVideoIntent();
                    } else {
                        ActivityCompat.requestPermissions(LiveReportActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_VIDEO_CAPTURE);

                    }
                }
                else {
                    Toast.makeText(LiveReportActivity.this,"Enter the title",Toast.LENGTH_SHORT).show();
                }


            }
        });


        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        imgrecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtTitle.getText().toString().equals("")) {

                    if (Utilities.checkPermission(LiveReportActivity.this, Manifest.permission.CAMERA)) {
                        dispatchTakeVideoIntent();
                    } else {
                        ActivityCompat.requestPermissions(LiveReportActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_VIDEO_CAPTURE);

                    }
                }
                else {
                    Toast.makeText(LiveReportActivity.this,"Enter the title",Toast.LENGTH_SHORT).show();

                }

            }
        });

        txtDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edtTitle.getText().toString().equals("")) {

                    if (Utilities.checkPermission(LiveReportActivity.this, Manifest.permission.CAMERA)) {
                        dispatchTakeVideoIntent();
                    } else {
                        ActivityCompat.requestPermissions(LiveReportActivity.this, new String[]{Manifest.permission.CAMERA}, REQUEST_VIDEO_CAPTURE);

                    }
                }
                else {
                    Toast.makeText(LiveReportActivity.this,"Enter the title",Toast.LENGTH_SHORT).show();

                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(grantResults.length>0||grantResults[0]== PackageManager.PERMISSION_GRANTED||requestCode==REQUEST_VIDEO_CAPTURE)
        {
            dispatchTakeVideoIntent();
        }



    }

    private void dispatchTakeVideoIntent() {
        Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
             videoUri = intent.getData();
           // videoView.setVideoURI(videoUri);
//            txtFilename.setVisibility(View.VISIBLE);
            btnrecord.setVisibility(View.GONE);
            layoutVideorecordbtn.setVisibility(View.VISIBLE);



            final MediaController mediacontroller = new MediaController(LiveReportActivity.this);
            mediacontroller.setAnchorView(videoview);




            videoview.setMediaController(mediacontroller);
            videoview.setVideoURI(videoUri);

            videoview.requestFocus();
            videoview.start();

            handler.postDelayed(
                    new Runnable() {
                        public void run() {
                            mediacontroller.show(0);
                        }},
                    100);


        }
    }


}
