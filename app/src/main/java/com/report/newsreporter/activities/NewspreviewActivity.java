package com.report.newsreporter.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.report.newsreporter.R;
import com.report.newsreporter.data.pojo.NewsPreview;
import com.report.newsreporter.data.pojo.NewsPreviewResponse;
import com.report.newsreporter.preferencehelper.PreferenceHelper;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.utils.Utilities;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.RetrofitHelperLogin;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewspreviewActivity extends AppCompatActivity{

    ImageView imgNews,imgEdit,imgback;
    TextView txtTitle,txtContent;
    VideoView videoview;
    NewsPreview newsPreview;
    Button btnSubmit;
    CardView videocard;
    ProgressFragment progressFragment;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_news_preview);
            getSupportActionBar().hide();
            newsPreview=(NewsPreview) getIntent().getSerializableExtra("newspreview");
            imgNews=findViewById(R.id.imgNews);
            imgback=findViewById(R.id.imgback);
            txtTitle=findViewById(R.id.txtTitle);
            txtContent=findViewById(R.id.txtContent);
            videoview=findViewById(R.id.videoview);
            imgEdit=findViewById(R.id.imgEdit);
            btnSubmit=findViewById(R.id.btnSubmit);
            videocard=findViewById(R.id.videocard);

            imgback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    onBackPressed();
                }
            });

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    showAlert();




                }
            });


            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    onBackPressed();
                }
            });


            if(newsPreview.getImagepath()!=null)
            {
                Glide.with(NewspreviewActivity.this)
                        .load(newsPreview.getImagepath())

                        .into(imgNews);
            }
            else {
                imgNews.setVisibility(View.GONE);
            }

            if(newsPreview.getVideouri()!=null)
            {
                MediaController mediacontroller = new MediaController(NewspreviewActivity.this);
                mediacontroller.setAnchorView(videoview);

                videoview.setVideoURI(Uri.parse(newsPreview.getVideouri()));
                videoview.setMediaController(mediacontroller);
                videoview.requestFocus();
                videoview.start();
            }
            else {
                videoview.setVisibility(View.GONE);
                videocard.setVisibility(View.GONE);
            }


            txtTitle.setText(newsPreview.getTitle());

            if(newsPreview.getCategory()!=null)
            {

                txtContent.append("Category : "+newsPreview.getCategory().getDsNewsCategory()+"\n");

            }
            if(!newsPreview.getPriority().equals(""))
            {

                txtContent.append("Priority : "+newsPreview.getPriority()+"\n");

            }
            txtContent.append(newsPreview.getContent());

        }

        public void showAlert(){


            AlertDialog.Builder builder=new AlertDialog.Builder(NewspreviewActivity.this);
            builder.setTitle(R.string.app_name);
            builder.setMessage("Do you want to post this news ?");
            builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                    submitNews();


                }
            });

            builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    dialogInterface.dismiss();

                }
            });
            builder.show();



        }

        public void submitNews()
        {

            progressFragment = new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(), "dklk");

            MultipartBody.Part body_image =null;
            MultipartBody.Part body_video =null;

            RequestBody title =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, newsPreview.getTitle());


            RequestBody content =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, newsPreview.getContent());

            RequestBody priority =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, newsPreview.getPriority());

            RequestBody category =
                    RequestBody.create(
                            okhttp3.MultipartBody.FORM, newsPreview.getCategory().getCdNewsCategory());


//            RequestBody requestFilepolice =
//                    RequestBody.create(
//                            MediaType.parse(Utilities.getMimeType(file_police)),
//                            file_police
//                    );
//
//            MultipartBody.Part body_police =
//                    MultipartBody.Part.createFormData("police_verification", file_police.getName(), requestFilepolice);
//

            if(newsPreview.getImagepath()!=null)
            {

                File fileimg=new File(newsPreview.getImagepath());
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(Utilities.getMimeType(fileimg)),
                                fileimg
                        );

                body_image =
                        MultipartBody.Part.createFormData("ds_image", new File(newsPreview.getImagepath()).getName(), requestFile);

            }

            if(newsPreview.getVideopath()!=null)
            {

                File filevideo=new File(newsPreview.getVideopath());
                RequestBody requestFilevideo =
                        RequestBody.create(
                                MediaType.parse(Utilities.getMimeType(filevideo)),
                                filevideo
                        );

               body_video =
                        MultipartBody.Part.createFormData("ds_video", new File(newsPreview.getVideopath()).getName(), requestFilevideo);

            }


            WebInterfaceimpl webInterfaceimpl= RetrofitHelper.getRetrofitInstance(NewspreviewActivity.this).create(WebInterfaceimpl.class);

            Call<List<NewsPreviewResponse>>jsonArrayCall=webInterfaceimpl.postNews(title,content,priority,category,body_image,body_video);

            jsonArrayCall.enqueue(new Callback<List<NewsPreviewResponse>>() {
                @Override
                public void onResponse(Call<List<NewsPreviewResponse>> call, Response<List<NewsPreviewResponse>> response) {

                    progressFragment.dismiss();

                    if(response.body()!=null)
                    {

                        if(response.body().size()>0)
                        {

                            if(response.body().get(0).getStatus()==1)
                            {
                                Toast.makeText(NewspreviewActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();


                                Intent intent1=new Intent(NewspreviewActivity.this,MainActivity.class);

                                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent1);
                                finish();

                            }
                            else {

                                Toast.makeText(NewspreviewActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();

                            }



                        }



                    }





                }

                @Override
                public void onFailure(Call<List<NewsPreviewResponse>> call, Throwable t) {
                    progressFragment.dismiss();

                }
            });


        }

}
