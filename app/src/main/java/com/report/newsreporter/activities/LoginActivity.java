package com.report.newsreporter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.report.newsreporter.R;
import com.report.newsreporter.connectivity.NetConnection;
import com.report.newsreporter.data.appconstants.Literals;
import com.report.newsreporter.data.pojo.Logindata;
import com.report.newsreporter.preferencehelper.PreferenceHelper;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.RetrofitHelperLogin;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    EditText edtPhone,edtPassword;
    Button btnlogin;

    TextView txtClickRegister;

    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        edtPhone=findViewById(R.id.edtPhone);
        edtPassword=findViewById(R.id.edtPassword);

        btnlogin=findViewById(R.id.btnlogin);
        txtClickRegister=findViewById(R.id.txtClickRegister);


        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtPhone.getText().toString().equals(""))
                {

                    if(!edtPassword.getText().toString().equals(""))
                    {

                        progressFragment=new ProgressFragment();
                        progressFragment.show(getSupportFragmentManager(),"cjkk");


                        WebInterfaceimpl webInterfaceimpl= RetrofitHelperLogin.getRetrofitInstance().create(WebInterfaceimpl.class);
                        Call<Logindata> jsonObjectCall=webInterfaceimpl.getLoginData(edtPhone.getText().toString(),edtPassword.getText().toString());
                        jsonObjectCall.enqueue(new Callback<Logindata>() {
                            @Override
                            public void onResponse(Call<Logindata> call, Response<Logindata> response) {
                                progressFragment.dismiss();

                                if(response.body().getStatus()==1)
                                {

                                    new PreferenceHelper(LoginActivity.this).putData(Literals.Logintokenkey,response.body().getToken());


                                    new PreferenceHelper(LoginActivity.this).putBooleanData(Literals.isRegistered,true);

                                    startActivity(new Intent(LoginActivity.this,MainActivity.class));
                                    finish();

                                }
                                else {

                                    Toast.makeText(LoginActivity.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();

                                }






                            }

                            @Override
                            public void onFailure(Call<Logindata> call, Throwable t) {
                                progressFragment.dismiss();


                                if(!NetConnection.isConnected(LoginActivity.this))
                                {
                                    Toast.makeText(LoginActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                                }


                            }
                        });




                    }
                    else {

                        Toast.makeText(LoginActivity.this,"Enter password",Toast.LENGTH_SHORT).show();
                    }


                }
                else {

                    Toast.makeText(LoginActivity.this,"Enter mobile number",Toast.LENGTH_SHORT).show();
                }

            }
        });

        txtClickRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this,TermsConditionsActivity.class));

                finish();

            }
        });

    }
}
