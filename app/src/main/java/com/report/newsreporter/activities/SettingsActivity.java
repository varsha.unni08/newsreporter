package com.report.newsreporter.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.report.newsreporter.R;
import com.report.newsreporter.data.pojo.District;
import com.report.newsreporter.data.pojo.PinCode;
import com.report.newsreporter.data.pojo.State;
import com.report.newsreporter.data.pojo.Village;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.RetrofitHelperLogin;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    AppCompatImageView imgback;

    TextView txtstate,txtDistrict,txtVillage,txtpin;

    ImageView imgDropdownstate,imgDropdowndist,imgDropdownvillage,imgDropdownpin;

    EditText edtPin;

    ProgressFragment progressFragment;
    State state;
    District district;
    PinCode pinCode;

    Button btnupdate;

    List<State>states=new ArrayList<>();
    List<District>districts=new ArrayList<>();

    Thread t=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().hide();
        imgback=findViewById(R.id.imgback);
        txtpin=findViewById(R.id.txtpin);
        txtstate=findViewById(R.id.txtstate);
        txtDistrict=findViewById(R.id.txtDistrict);
        txtVillage=findViewById(R.id.txtVillage);
        btnupdate=findViewById(R.id.btnupdate);

        imgDropdownstate=findViewById(R.id.imgDropdownstate);
        imgDropdowndist=findViewById(R.id.imgDropdowndist);
        imgDropdownvillage=findViewById(R.id.imgDropdownvillage);
        imgDropdownpin=findViewById(R.id.imgDropdownpin);

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });

        txtstate.setOnClickListener(SettingsActivity.this);
        txtDistrict.setOnClickListener(SettingsActivity.this);
        txtVillage.setOnClickListener(SettingsActivity.this);
        txtpin.setOnClickListener(SettingsActivity.this);
        imgDropdownstate.setOnClickListener(SettingsActivity.this);
        imgDropdowndist.setOnClickListener(SettingsActivity.this);
        imgDropdownvillage.setOnClickListener(SettingsActivity.this);
        imgDropdownpin.setOnClickListener(SettingsActivity.this);

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final ProgressFragment progressFragment=new ProgressFragment();
                progressFragment.show(getSupportFragmentManager(),"zxkj");

              t=   new Thread(new Runnable() {
                    @Override
                    public void run() {

                        try{

                            t.sleep(3000);
                            progressFragment.dismiss();


                        }catch (Exception e)
                        {

                        }

                    }
                });

             t.start();



            }
        });



    }

    @Override
    public void onClick(View view) {

        switch (view.getId())
        {

         case   R.id.imgDropdownstate:
             getState();

            break;

            case   R.id.imgDropdowndist:

                if(state!=null) {
                    CallDistrict(state.getCdState());
                }
                break;

            case   R.id.imgDropdownvillage:
                if(pinCode!=null) {
//                    showVillageAlert();
                    callVillage();
                }

                break;


            case   R.id.txtstate:
                getState();

                break;

            case   R.id.txtDistrict:
                if(state!=null) {
                    CallDistrict(state.getCdState());
                }

                break;

            case   R.id.txtVillage:
                if(pinCode!=null) {
//                    showVillageAlert();
                    callVillage();
                }

                break;

            case R.id.imgDropdownpin:

                if(district!=null) {

                    callPin(district.getCdDistrict());
                }

                break;

            case R.id.txtpin:
                if(district!=null) {

                    callPin(district.getCdDistrict());
                }

                break;







        }

    }

    public void getState()
    {

        if(states.size()==0) {


            progressFragment = new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(), "cjkk");
            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(SettingsActivity.this).create(WebInterfaceimpl.class);

            Call<List<State>> jsonArrayCall = webInterfaceimpl.getState();
            jsonArrayCall.enqueue(new Callback<List<State>>() {
                @Override
                public void onResponse(Call<List<State>> call, final Response<List<State>> response) {
                    progressFragment.dismiss();
                    Log.e("STATE", response.body().toString());

                    if (response.body() != null) {

                        states.addAll(response.body());

                        showStateAlert(response.body());
                    }


                }

                @Override
                public void onFailure(Call<List<State>> call, Throwable t) {
                    progressFragment.dismiss();

                }
            });

        }
        else {

            showStateAlert(states);

        }


    }

    public void showStateAlert(final List<State>states)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle("Choose a state");
        for (State st:states)
        {
            strings.add(st.getDsState());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                state=states.get(which);

                txtstate.setText(state.getDsState());

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void CallDistrict(String st_id )
    {

            progressFragment = new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(), "cjkk");
            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(SettingsActivity.this).create(WebInterfaceimpl.class);

            Call<List<District>> jsonArrayCall = webInterfaceimpl.getDistrict(st_id);
            jsonArrayCall.enqueue(new Callback<List<District>>() {
                @Override
                public void onResponse(Call<List<District>> call, final Response<List<District>> response) {
                    progressFragment.dismiss();
                    Log.e("district", response.body().toString());

                    if (response.body() != null) {

showDistrictAlert(response.body());
                    }


                }

                @Override
                public void onFailure(Call<List<District>> call, Throwable t) {
                    progressFragment.dismiss();

                }
            });

    }

    public void showDistrictAlert(final List<District>dist)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle("Choose a district");
        for (District st:dist)
        {
            strings.add(st.getDsDistrict());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                district=dist.get(which);

                txtDistrict.setText(district.getDsDistrict());

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void callPin(String distid)
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");
        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(SettingsActivity.this).create(WebInterfaceimpl.class);

        Call<List<PinCode>> jsonArrayCall = webInterfaceimpl.getPincode(distid);
        jsonArrayCall.enqueue(new Callback<List<PinCode>>() {
            @Override
            public void onResponse(Call<List<PinCode>> call, final Response<List<PinCode>> response) {
                progressFragment.dismiss();
             //   Log.e("pin", response.body().toString());

                if (response.body() != null) {

                    //showDistrictAlert(response.body());
                    showPinAlert(response.body());
                }


            }

            @Override
            public void onFailure(Call<List<PinCode>> call, Throwable t) {
                progressFragment.dismiss();

            }
        });
    }

    public void showPinAlert(final List<PinCode>dist)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle("Choose a pin code");
        for (PinCode st:dist)
        {
            strings.add(st.getDsPincode());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                pinCode=dist.get(which);

                txtpin.setText(pinCode.getDsPincode());


            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void callVillage()
    {
        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "cjkk");
        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(SettingsActivity.this).create(WebInterfaceimpl.class);

        Call<List<Village>>jsonArrayCall=webInterfaceimpl.getVillage(pinCode.getCdPincode());
        jsonArrayCall.enqueue(new Callback<List<Village>>() {
            @Override
            public void onResponse(Call<List<Village>> call, Response<List<Village>> response) {

                progressFragment.dismiss();
                if(response.code()==200)
                {
                    if(response.body()!=null)
                    {

                        showVillageAlert(response.body());



                    }




                }


            }

            @Override
            public void onFailure(Call<List<Village>> call, Throwable t) {
                progressFragment.dismiss();

            }
        });
    }


    public void showVillageAlert(List<Village>villages)
    {

        final List<String>strings=new ArrayList<>();

        AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);
        builder.setTitle("Choose a village");
  ;
        for (Village st:villages)
        {
            strings.add(st.getDsVillage());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

                txtVillage.setText(strings.get(which));

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }





}
