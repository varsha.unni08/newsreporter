package com.report.newsreporter.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.report.newsreporter.R;
import com.report.newsreporter.connectivity.NetConnection;
import com.report.newsreporter.data.appconstants.Literals;
import com.report.newsreporter.data.pojo.Logindata;
import com.report.newsreporter.data.pojo.OtpGenerateData;
import com.report.newsreporter.data.pojo.RegisterData;
import com.report.newsreporter.preferencehelper.PreferenceHelper;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.utils.Utilities;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.RetrofitHelperLogin;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {

    ProgressFragment progressFragment;

    public static final int forprofile = 12, forpolice = 10, forqualification = 19, foradhar = 11, signin = 112;

    ImageView imgback, imgprofile, imgpickDate, imgDropdown, imgpoliceverification, imgQualificationproof, imgAdharproof, imgGoogle, imgsubmit;

    EditText edtQualification, edtAdhar, edtEmail, edtName, edtPin, edtPassword, edtConfirmPassword, edtMobile, edtTransaction;

    TextView txtDob, txtGender, txtPoliceVerification, txtQualification, txtAdharverification, txtfromgoogle;

    File file_profile, file_police, file_qualification, file_adhar;

    String dob = "", gender = "";
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().hide();

        initViews();
        setClickEvents();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.webclientid))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.imgprofile:

                if (Utilities.checkPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(forprofile);
                } else {

                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, forprofile);
                }


                break;

            case R.id.imgback:
                onBackPressed();


                break;

            case R.id.imgpoliceverification:

                if (Utilities.checkPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(forpolice);
                } else {

                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, forpolice);
                }
                break;

            case R.id.imgQualificationproof:

                if (Utilities.checkPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(forqualification);
                } else {

                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, forqualification);
                }

                break;

            case R.id.imgAdharproof:

                if (Utilities.checkPermission(RegisterActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(foradhar);
                } else {

                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, foradhar);
                }

                break;

            case R.id.imgpickDate:

                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);

                DatePickerDialog datePickerDialog = new DatePickerDialog(RegisterActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                        // txtDob.setText();

                        int m=i1+1;
                        dob = i + "-" + m + "-" + i2;
                        txtDob.setText(dob);

                    }
                }, year, month, day);

                datePickerDialog.show();


                break;


            case R.id.txtGender:

                showGenderDialog();

                break;

            case R.id.imgDropdown:

                showGenderDialog();

                break;

            case R.id.imgGoogle:


                if (Utilities.checkPermission(RegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, signin);
                } else {


                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, signin);


                }


                break;

            case R.id.txtfromgoogle:


                if (Utilities.checkPermission(RegisterActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, signin);
                } else {


                    ActivityCompat.requestPermissions(RegisterActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, signin);


                }

                break;

            case R.id.imgsubmit:

                SubmitData();

                break;


        }
    }


    public void SubmitData() {

        if (file_profile != null) {

            if (!edtName.getText().toString().equals("")) {

                if (!dob.equals("")) {

                    if (!gender.equals("")) {

                        if (file_police != null) {

                            if (!edtQualification.getText().toString().equals("")) {

                                if (file_qualification != null) {

                                    if (!edtAdhar.getText().toString().equals("")) {

                                        if (file_adhar != null) {

                                            if (!edtPin.getText().toString().equals("")) {


                                                if (!edtPassword.getText().toString().equals("")) {

                                                    if (!edtConfirmPassword.getText().toString().equals("")) {


                                                        if (edtConfirmPassword.getText().toString().equals(edtPassword.getText().toString())) {

                                                            if (!edtMobile.getText().toString().equals("")) {


                                                                if (!edtTransaction.getText().toString().equals("")) {


                                                                    submitAllDataToServer();


                                                                } else {

                                                                    Toast.makeText(RegisterActivity.this, "enter transaction details", Toast.LENGTH_SHORT).show();


                                                                }


                                                            } else {

                                                                Toast.makeText(RegisterActivity.this, "enter mobile", Toast.LENGTH_SHORT).show();


                                                            }
                                                        } else {


                                                            Toast.makeText(RegisterActivity.this, "confirm password", Toast.LENGTH_SHORT).show();


                                                        }


                                                    } else {


                                                        Toast.makeText(RegisterActivity.this, "confirm password", Toast.LENGTH_SHORT).show();


                                                    }


                                                } else {


                                                    Toast.makeText(RegisterActivity.this, "enter password", Toast.LENGTH_SHORT).show();


                                                }


                                            } else {


                                                Toast.makeText(RegisterActivity.this, "enter pin code", Toast.LENGTH_SHORT).show();


                                            }


                                        } else {


                                            Toast.makeText(RegisterActivity.this, "upload adhar file", Toast.LENGTH_SHORT).show();


                                        }


                                    } else {


                                        Toast.makeText(RegisterActivity.this, "Enter adhar number", Toast.LENGTH_SHORT).show();


                                    }


                                } else {

                                    Toast.makeText(RegisterActivity.this, "upload qualification file", Toast.LENGTH_SHORT).show();

                                }


                            } else {

                                Toast.makeText(RegisterActivity.this, "Enter your qualification", Toast.LENGTH_SHORT).show();

                            }


                        } else {

                            Toast.makeText(RegisterActivity.this, "upload police verification proof", Toast.LENGTH_SHORT).show();

                        }


                    } else {

                        Toast.makeText(RegisterActivity.this, "Select gender", Toast.LENGTH_SHORT).show();

                    }


                } else {

                    Toast.makeText(RegisterActivity.this, "Select date of birth", Toast.LENGTH_SHORT).show();
                }


            } else {

                Toast.makeText(RegisterActivity.this, "Enter name", Toast.LENGTH_SHORT).show();
            }


        } else {

            Toast.makeText(RegisterActivity.this, "Upload profile image", Toast.LENGTH_SHORT).show();
        }


    }


    public void submitAllDataToServer() {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dklk");
        WebInterfaceimpl webInterfaceimpl = RetrofitHelperLogin.getRetrofitInstance().create(WebInterfaceimpl.class);


        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse(Utilities.getMimeType(file_profile)),
                        file_profile
                );

        MultipartBody.Part body_profile =
                MultipartBody.Part.createFormData("ds_employee_image", file_profile.getName(), requestFile);

        RequestBody email =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtEmail.getText().toString());
        RequestBody name =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtName.getText().toString());
        RequestBody date =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, dob);
        RequestBody gender_rquest =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, gender);

        RequestBody requestFilepolice =
                RequestBody.create(
                        MediaType.parse(Utilities.getMimeType(file_police)),
                        file_police
                );

        MultipartBody.Part body_police =
                MultipartBody.Part.createFormData("police_verification", file_police.getName(), requestFilepolice);


        RequestBody qualification_rquest =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtQualification.getText().toString());


        RequestBody requestFilequali =
                RequestBody.create(
                        MediaType.parse(Utilities.getMimeType(file_qualification)),
                        file_qualification
                );

        MultipartBody.Part body_qualification =
                MultipartBody.Part.createFormData("ds_qualification_proof", file_qualification.getName(), requestFilequali);


        RequestBody adhar_rquest =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtAdhar.getText().toString());


        RequestBody requestFileadhar =
                RequestBody.create(
                        MediaType.parse(Utilities.getMimeType(file_adhar)),
                        file_adhar
                );

        MultipartBody.Part body_adhar =
                MultipartBody.Part.createFormData("ds_adress_proof1", file_adhar.getName(), requestFileadhar);


        RequestBody pin_rquest =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtPin.getText().toString());

        RequestBody pin_password =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtPassword.getText().toString());

        RequestBody pin_mobile =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtMobile.getText().toString());

        RequestBody pin_transaction =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, edtTransaction.getText().toString());



        Call<List<RegisterData>> jsonObjectCall = webInterfaceimpl.getRegistration(name,pin_rquest,pin_mobile,qualification_rquest,body_qualification,adhar_rquest,body_adhar,email,date,gender_rquest,pin_password,body_profile,body_police,pin_transaction);

        jsonObjectCall.enqueue(new Callback<List<RegisterData>>() {
            @Override
            public void onResponse(Call<List<RegisterData>> call, Response<List<RegisterData>> response) {

                progressFragment.dismiss();

//                Log.e("TAG",response.body().toString());

                if(response.code()==200)
                {

                   if(response.body().size()>0)
                   {

                       new PreferenceHelper(RegisterActivity.this).putData(Literals.Logintokenkey,response.body().get(0).getToken());

                       Toast.makeText(RegisterActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();


                       generateOtp();

                   }
                }


            }

            @Override
            public void onFailure(Call<List<RegisterData>> call, Throwable t) {
                progressFragment.dismiss();
                Log.e("ETAG",t.toString());
                if(!NetConnection.isConnected(RegisterActivity.this))
                {
                    Toast.makeText(RegisterActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });

    }


    public void showGenderDialog() {

        final String arr[] = {"Male", "Female"};
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(this);
        //alt_bld.setIcon(R.drawable.icon);
        alt_bld.setTitle("Select a Group Name");
        alt_bld.setSingleChoiceItems(arr, -1, new DialogInterface
                .OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                gender = arr[item];
                txtGender.setText(gender);

                dialog.dismiss();// dismiss the alertbox after chose option

            }
        });
        AlertDialog alert = alt_bld.create();
        alert.show();
    }


    // clientid: 787965208405-ok2knqdhqrrslbq02slsqqf68q702ame.apps.googleusercontent.com

    //client_secret :  5De41b-onWBgGi25zHQjCYOn
    public void initViews() {

        imgprofile = findViewById(R.id.imgprofile);
        imgback = findViewById(R.id.imgback);
        imgpickDate = findViewById(R.id.imgpickDate);
        imgDropdown = findViewById(R.id.imgDropdown);
        imgpoliceverification = findViewById(R.id.imgpoliceverification);
        imgQualificationproof = findViewById(R.id.imgQualificationproof);
        imgAdharproof = findViewById(R.id.imgAdharproof);
        imgGoogle = findViewById(R.id.imgGoogle);
        imgsubmit = findViewById(R.id.imgsubmit);

        edtEmail = findViewById(R.id.edtEmail);
        edtName = findViewById(R.id.edtName);
        edtPin = findViewById(R.id.edtPin);
        edtPassword = findViewById(R.id.edtPassword);
        edtConfirmPassword = findViewById(R.id.edtConfirmPassword);
        edtMobile = findViewById(R.id.edtMobile);
        edtTransaction = findViewById(R.id.edtTransaction);
        edtQualification = findViewById(R.id.edtQualification);
        edtAdhar = findViewById(R.id.edtQualification);

        txtDob = findViewById(R.id.txtDob);
        txtGender = findViewById(R.id.txtGender);
        txtPoliceVerification = findViewById(R.id.txtPoliceVerification);
        txtQualification = findViewById(R.id.txtQualification);
        txtAdharverification = findViewById(R.id.txtAdharverification);
        txtfromgoogle = findViewById(R.id.txtfromgoogle);
    }


    public void setClickEvents() {

        imgback.setOnClickListener(RegisterActivity.this);
        imgprofile.setOnClickListener(RegisterActivity.this);
        imgpickDate.setOnClickListener(RegisterActivity.this);
        imgDropdown.setOnClickListener(RegisterActivity.this);
        imgpoliceverification.setOnClickListener(RegisterActivity.this);
        imgQualificationproof.setOnClickListener(RegisterActivity.this);
        imgAdharproof.setOnClickListener(RegisterActivity.this);
        imgGoogle.setOnClickListener(RegisterActivity.this);
        imgsubmit.setOnClickListener(RegisterActivity.this);


        txtDob = findViewById(R.id.txtDob);
        txtGender = findViewById(R.id.txtGender);
        txtPoliceVerification = findViewById(R.id.txtPoliceVerification);
        txtQualification = findViewById(R.id.txtQualification);
        txtAdharverification = findViewById(R.id.txtAdharverification);
        txtfromgoogle = findViewById(R.id.txtfromgoogle);


        txtDob.setOnClickListener(RegisterActivity.this);
        txtGender.setOnClickListener(RegisterActivity.this);
        txtPoliceVerification.setOnClickListener(RegisterActivity.this);
        txtQualification.setOnClickListener(RegisterActivity.this);
        txtAdharverification.setOnClickListener(RegisterActivity.this);
        txtfromgoogle.setOnClickListener(RegisterActivity.this);


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {

            if (requestCode != signin) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);
                cursor.close();

                switch (requestCode) {


                    case forprofile:

                        file_profile = new File(picturePath);
                        Glide.with(RegisterActivity.this)
                                .load(file_profile)
                                .apply(RequestOptions.circleCropTransform())
                                .into(imgprofile);

                        break;

                    case forpolice:

                        file_police = new File(picturePath);
                        txtPoliceVerification.setText(file_police.getName());


                        break;

                    case forqualification:

                        file_qualification = new File(picturePath);
                        txtQualification.setText(file_qualification.getName());

                        break;

                    case foradhar:

                        file_adhar = new File(picturePath);
                        txtAdharverification.setText(file_adhar.getName());

                        break;


                }
            } else {

                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);

                    if (account != null) {
                        if (account.getDisplayName() != null) {
                            edtName.setText(account.getDisplayName());

                        }

                        if (account.getEmail() != null) {
                            edtEmail.setText(account.getEmail());
                        }

                        if (account.getPhotoUrl() != null) {

                            new DownloadImageAsync(account.getPhotoUrl().toString()).execute("");
                        }
                    }


                    //  Toast.makeText(RegisterActivity.this, account.getEmail(), Toast.LENGTH_SHORT).show();
                    // firebaseAuthWithGoogle(account);
                } catch (ApiException e) {
                    // Google Sign In failed, update UI appropriately
                    Log.w("fk", "Google sign in failed", e);
                    // ...
                }

            }

            // String picturePath contains the path of selected Image
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 || grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode != signin) {
                pickImage(requestCode);
            } else {

                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, signin);
            }
        }
    }

    public void pickImage(int code) {
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, code);
    }


    public class DownloadImageAsync extends AsyncTask<String, String, String> {


        String url_photo;

        public DownloadImageAsync(String url_photo) {
            this.url_photo = url_photo;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressFragment = new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(), "jdfskj");
        }

        @Override
        protected String doInBackground(String... strings) {
            File dest = null;
            try {
                java.net.URL url = new java.net.URL(url_photo);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);

                Bitmap resizedBitmap = getResizedBitmap(myBitmap, myBitmap.getWidth(), myBitmap.getHeight());

                String filename = "img_" + Calendar.getInstance().getTimeInMillis() + ".png";
                File sd = Environment.getExternalStorageDirectory();
                dest = new File(sd, filename);

                Bitmap bitmap = resizedBitmap;
                try {
                    FileOutputStream out = new FileOutputStream(dest);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
                    out.flush();
                    out.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }


            return dest.getAbsolutePath();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressFragment.dismiss();


            file_profile = new File(s);

            Glide.with(RegisterActivity.this)
                    .load(file_profile)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imgprofile);


        }
    }


    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }

    public void generateOtp()
    {

        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dklk");
        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(RegisterActivity.this).create(WebInterfaceimpl.class);

        Call<List<OtpGenerateData>>jsonArrayCall=webInterfaceimpl.generateOTp();
        jsonArrayCall.enqueue(new Callback<List<OtpGenerateData>>() {
            @Override
            public void onResponse(Call<List<OtpGenerateData>> call, Response<List<OtpGenerateData>> response) {

                progressFragment.dismiss();
                if(response.body()!=null)
                {
                    if(response.code()==200)
                    {

                        //Log.e("Otpgenerate",response.body().toString());

                        if(response.body().size()>0)
                        {

                            if(response.body().get(0).getStatus()==1)
                            {


                                Toast.makeText(RegisterActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();


                                startActivity(new Intent(RegisterActivity.this,OTPActivity.class).putExtra("Email",edtEmail.getText().toString()));

                            }
                            else {

                                Toast.makeText(RegisterActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();

                            }
                        }



                    }
                }


            }

            @Override
            public void onFailure(Call<List<OtpGenerateData>> call, Throwable t) {

                progressFragment.dismiss();
                if(!NetConnection.isConnected(RegisterActivity.this))
                {
                    Toast.makeText(RegisterActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });


    }


}
