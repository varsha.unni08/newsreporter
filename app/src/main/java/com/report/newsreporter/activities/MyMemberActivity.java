package com.report.newsreporter.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.report.newsreporter.R;
import com.report.newsreporter.utils.Utilities;

import java.io.File;

public class MyMemberActivity extends AppCompatActivity {

    AppCompatImageView imgback;
    Button btnrecord;
    AppCompatImageView imgView;
    LinearLayout layoutImgupload;
    TextView txtUpload;
    AppCompatImageView imgupload;
    int foradhar=102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_member);
        getSupportActionBar().hide();

        imgback=findViewById(R.id.imgback);
        btnrecord=findViewById(R.id.btnrecord);
        imgView=findViewById(R.id.imgView);
        layoutImgupload=findViewById(R.id.layoutImgupload);
        txtUpload=findViewById(R.id.txtUpload);
        imgupload=findViewById(R.id.imgupload);

        imgupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utilities.checkPermission(MyMemberActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(foradhar);
                } else {

                    ActivityCompat.requestPermissions(MyMemberActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, foradhar);
                }

            }
        });





        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });
        txtUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utilities.checkPermission(MyMemberActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(foradhar);
                } else {

                    ActivityCompat.requestPermissions(MyMemberActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, foradhar);
                }

            }
        });

        btnrecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.checkPermission(MyMemberActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(foradhar);
                } else {

                    ActivityCompat.requestPermissions(MyMemberActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, foradhar);
                }


            }
        });
    }

    public void pickImage(int code) {
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, code);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {

            if (requestCode == foradhar) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);

               File file_profile = new File(picturePath);
                Glide.with(MyMemberActivity.this)
                        .load(file_profile)

                        .into(imgView);
                cursor.close();

                layoutImgupload.setVisibility(View.VISIBLE);
                btnrecord.setVisibility(View.GONE);

            }
        }
    }
}
