package com.report.newsreporter.activities;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.gson.JsonArray;
import com.report.newsreporter.R;
import com.report.newsreporter.adapters.NewsReportAdapter;
import com.report.newsreporter.connectivity.NetConnection;
import com.report.newsreporter.data.pojo.NewsReport;
import com.report.newsreporter.data.pojo.ReporterProfile;
import com.report.newsreporter.preferencehelper.PreferenceHelper;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Menu;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    ProgressFragment progressFragment;

    View view;

    TextView txtname,txtEmail,txtdatemessage,txtnodata;
    ImageView imgprofile,imgsettings,imgdrawer,imgclose,imgfindreporter;

    ReporterProfile Profile=null;

    DrawerLayout drawer;
    RecyclerView recycler_view;
    FloatingActionButton floatingActionButton;

    LinearLayout layoutdate;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        layoutdate=findViewById(R.id.layoutdate);
        txtdatemessage=findViewById(R.id.txtdatemessage);
        imgclose=findViewById(R.id.imgclose);
        imgfindreporter=findViewById(R.id.imgfindreporter);

        imgsettings=findViewById(R.id.imgsettings);
        imgdrawer=findViewById(R.id.imgdrawer);
        recycler_view=findViewById(R.id.recycler_view);
        txtnodata=findViewById(R.id.txtnodata);
        floatingActionButton=findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent2=new Intent(MainActivity.this,AddNewReportActivity.class);

                startActivity(intent2);


            }
        });
        imgfindreporter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                startActivity(new Intent(MainActivity.this,ReporterSearchActivity.class));
            }
        });

        imgsettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(MainActivity.this,SettingsActivity.class));


            }
        });

         drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        t = new ActionBarDrawerToggle(this, drawer,R.string.open, R.string.close);
//
//        drawer.addDrawerListener(t);
//        t.syncState();

        imgdrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.openDrawer(Gravity.LEFT);

            }
        });



        view=navigationView.getHeaderView(0);
        imgprofile=view.findViewById(R.id.imgprofile);
        txtname=view.findViewById(R.id.txtName);
        txtEmail=view.findViewById(R.id.txtEmail);

        imgprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.closeDrawer(Gravity.LEFT);

                startActivity(new Intent(MainActivity.this,ProfileActivity.class).putExtra("Reporterprofile",Profile));

            }
        });

        txtname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(MainActivity.this,ProfileActivity.class).putExtra("Reporterprofile",Profile));


            }
        });
        txtEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawer.closeDrawer(Gravity.LEFT);
                startActivity(new Intent(MainActivity.this,ProfileActivity.class).putExtra("Reporterprofile",Profile));


            }
        });


//        mAppBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
//                R.id.nav_tools, R.id.nav_share)
//                .setDrawerLayout(drawer)
//                .build();
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
//        NavigationUI.setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                drawer.closeDrawer(Gravity.LEFT);

                switch (menuItem.getItemId())
                {

                  case  R.id.nav_emergencycall:

//                      Intent intent = new Intent(Intent.ACTION_DIAL);
//                      intent.setData(Uri.parse("tel:8660481428"));
//                      startActivity(intent);

                    Dialog  dialog=new Dialog(MainActivity.this);
                      dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                      dialog.setContentView(R.layout.layout_calltypes);

                      ImageView imgcall=(ImageView) dialog.findViewById(R.id.imgcall);
                      TextView txtCall=(TextView)dialog.findViewById(R.id.txtCall);

                      CardView cardcall=(CardView)dialog.findViewById(R.id.cardcall);

                      imgcall.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              Intent intent = new Intent(Intent.ACTION_DIAL);
                              intent.setData(Uri.parse("tel:8660481428"));
                              startActivity(intent);
                          }
                      });

                      txtCall.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              Intent intent = new Intent(Intent.ACTION_DIAL);
                              intent.setData(Uri.parse("tel:8660481428"));
                              startActivity(intent);
                          }
                      });


                      cardcall.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View view) {
                              Intent intent = new Intent(Intent.ACTION_DIAL);
                              intent.setData(Uri.parse("tel:8660481428"));
                              startActivity(intent);
                          }
                      });


                      DisplayMetrics displayMetrics = new DisplayMetrics();
                     getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                      int widthLcl = (int) (displayMetrics.widthPixels);
                      int heightLcl =(int) (displayMetrics.heightPixels);

                      dialog.getWindow().setLayout(widthLcl,heightLcl);

                      dialog.show();





                      break;
                    case R.id.nav_logout:

                        AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
                        builder.setTitle(R.string.app_name);
                        builder.setMessage("Do you want to logout now ?");
                        builder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                new PreferenceHelper(MainActivity.this).clearData();

                                Intent intent1=new Intent(MainActivity.this,LoginActivity.class);

                                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent1);
                                finish();

                            }
                        });

                        builder.setNegativeButton("no", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();

                            }
                        });
                        builder.show();


                        break;

                    case R.id.nav_livereport:

                        Intent intent1=new Intent(MainActivity.this,LiveReportActivity.class);

                        startActivity(intent1);

                        break;
                    case R.id.nav_mymember:

                        Intent intent2=new Intent(MainActivity.this,MyMemberActivity.class);

                        startActivity(intent2);



                        break;

                    case R.id.nav_search:



                        showDatePickerDialog();


                        break;


                }



                return true;
            }
        });

        getProfile();
        showAllNews();

        imgclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                layoutdate.setVisibility(View.GONE);
                txtnodata.setVisibility(View.GONE);
                recycler_view.setVisibility(View.VISIBLE);


                showAllNews();
            }
        });
    }



//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
//                || super.onSupportNavigateUp();
//    }

    @Override
    protected void onRestart() {
        super.onRestart();

        getProfile();
    }


    public void showDatePickerDialog()
    {
        final Calendar cldr = Calendar.getInstance();
        int day = cldr.get(Calendar.DAY_OF_MONTH);
        int month = cldr.get(Calendar.MONTH);
        int year = cldr.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

                int month=i1+1;

                String date=i+"-"+month+"-"+i2;
                showNewsBydate(date);





            }
        }, year, month, day);

        datePickerDialog.show();
    }

    public void showNewsBydate(final String date)
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"sdjfkdj");


        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(MainActivity.this).create(WebInterfaceimpl.class);

        Call<List<NewsReport>>listCall=webInterfaceimpl.getSearchNewsByDate(date);
        listCall.enqueue(new Callback<List<NewsReport>>() {
            @Override
            public void onResponse(Call<List<NewsReport>> call, Response<List<NewsReport>> response) {

                progressFragment.dismiss();

                layoutdate.setVisibility(View.VISIBLE);
                txtdatemessage.setText("Searched date : "+date);

                if(response.body()!=null)
                {

                    if(response.body().size()>0) {

                        recycler_view.setVisibility(View.VISIBLE);
                        txtnodata.setVisibility(View.GONE);
                        NewsReportAdapter newsReportAdapter = new NewsReportAdapter(MainActivity.this, response.body());
                        recycler_view.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        recycler_view.setAdapter(newsReportAdapter);
                    }
                    else {
                        Toast.makeText(MainActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                        recycler_view.setVisibility(View.GONE);
                        txtnodata.setVisibility(View.VISIBLE);



                    }
                }
                else {

                    Toast.makeText(MainActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                    recycler_view.setVisibility(View.GONE);
                    txtnodata.setVisibility(View.VISIBLE);

                }



            }

            @Override
            public void onFailure(Call<List<NewsReport>> call, Throwable t) {

                progressFragment.dismiss();
                if(!NetConnection.isConnected(MainActivity.this))
                {
                    Toast.makeText(MainActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });

    }

    public void getProfile()
    {



        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(MainActivity.this).create(WebInterfaceimpl.class);

        Call<List<ReporterProfile>>jsonArrayCall=webInterfaceimpl.getReporterProfile();
        jsonArrayCall.enqueue(new Callback<List<ReporterProfile>>() {
            @Override
            public void onResponse(Call<List<ReporterProfile>> call, Response<List<ReporterProfile>> response) {


              if(response.body()!=null)
              {

                  if(response.body().size()>0)
                  {
                      ReporterProfile reporterProfile=response.body().get(0);
                    Profile=reporterProfile;

                      Glide.with(MainActivity.this)
                              .load(reporterProfile.getDsEmployeeImage())
                              .apply(RequestOptions.circleCropTransform())
                              .placeholder(R.drawable.user)
                              .into(imgprofile);

                      txtEmail.setText(reporterProfile.getDsEmail());
                      txtname.setText(reporterProfile.getDsReporterUser());




                  }



              }




            }

            @Override
            public void onFailure(Call<List<ReporterProfile>> call, Throwable t) {


                if(!NetConnection.isConnected(MainActivity.this))
                {
                    Toast.makeText(MainActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }


            }
        });


    }


    public void showAllNews()
    {

        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"kdfmli");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(MainActivity.this).create(WebInterfaceimpl.class);

        final Call<List<NewsReport>>jsonArrayCall=webInterfaceimpl.getReporterNews();
        jsonArrayCall.enqueue(new Callback<List<NewsReport>>() {
            @Override
            public void onResponse(Call<List<NewsReport>> call, Response<List<NewsReport>> response) {
                progressFragment.dismiss();


                if(response.body()!=null) {

                    if(response.body().size()>0) {



                        NewsReportAdapter newsReportAdapter = new NewsReportAdapter(MainActivity.this, response.body());
                        recycler_view.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        recycler_view.setAdapter(newsReportAdapter);
                    }
                    else {

                        Toast.makeText(MainActivity.this,"No data found",Toast.LENGTH_SHORT).show();

                        recycler_view.setVisibility(View.GONE);
                        txtnodata.setVisibility(View.VISIBLE);
                    }
                }
                else {

                    Toast.makeText(MainActivity.this,"No data found",Toast.LENGTH_SHORT).show();
                    recycler_view.setVisibility(View.GONE);
                    txtnodata.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<List<NewsReport>> call, Throwable t) {

                progressFragment.dismiss();
                if(!NetConnection.isConnected(MainActivity.this))
                {
                    Toast.makeText(MainActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }



            }
        });
    }




}
