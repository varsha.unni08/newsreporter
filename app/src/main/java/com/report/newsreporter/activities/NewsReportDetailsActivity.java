package com.report.newsreporter.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.report.newsreporter.R;
import com.report.newsreporter.data.pojo.NewsReport;

public class NewsReportDetailsActivity extends AppCompatActivity {

    AppCompatImageView imgback;
    ImageView imgNews,imgAdvertisement,imgShare;
    TextView txtTitle,txtViews,txtContent;
    NewsReport newsReport;
    VideoView videoview;
    ScrollView scroll;
    ImageButton imgbtnClose;
    RatingBar ratingbar;
    Button btnundo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_report_details);
        getSupportActionBar().hide();
        newsReport=(NewsReport)getIntent().getSerializableExtra("newsreport");
        imgback=findViewById(R.id.imgback);
        imgAdvertisement=findViewById(R.id.imgAdvertisement);
        imgbtnClose=findViewById(R.id.imgbtnClose);
        scroll=findViewById(R.id.scroll);
        txtTitle=findViewById(R.id.txtTitle);
        btnundo=findViewById(R.id.btnundo);
        txtViews=findViewById(R.id.txtViews);
        txtContent=findViewById(R.id.txtContent);
        imgNews=findViewById(R.id.imgNews);
        imgShare=findViewById(R.id.imgShare);
        videoview=findViewById(R.id.videoview);
        ratingbar=findViewById(R.id.ratingbar);
        Handler handler=new Handler();


        imgbtnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgAdvertisement.setVisibility(View.GONE);
                imgbtnClose.setVisibility(View.GONE);
                btnundo.setVisibility(View.VISIBLE);
            }
        });

        btnundo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                imgAdvertisement.setVisibility(View.VISIBLE);
                imgbtnClose.setVisibility(View.VISIBLE);
                btnundo.setVisibility(View.GONE);
            }
        });


        imgShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String shareBody = newsReport.getDsNewsHead()+"\n"+newsReport.getDsNewsContent()+"\n Photo :"+newsReport.getDsImage()+"\n Video : "+newsReport.getDsVideo() ;
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, newsReport.getDsNewsHead());
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent,"Share News via ..."));

            }
        });





        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


        if(newsReport!=null) {

            if(!newsReport.getDsImage().equals("")) {

                Glide.with(NewsReportDetailsActivity.this)
                        .load(newsReport.getDsImage())

                        .placeholder(R.drawable.picture)
                        .into(imgNews);


            }
            txtTitle.setText(newsReport.getDsNewsHead());

            if(!newsReport.getTotalReviews().equals("")) {

             txtViews.setText(newsReport.getTotalReviews()+" Reviews");
            }

            if(!newsReport.getDsNewsContent().equals(""))
            {
                txtContent.setText(newsReport.getDsNewsContent());
            }

            final MediaController mediacontroller = new MediaController(NewsReportDetailsActivity.this);
            mediacontroller.setAnchorView(videoview);




            videoview.setMediaController(mediacontroller);
            videoview.setVideoURI(Uri.parse(newsReport.getDsVideo()));



videoview.start();

            handler.postDelayed(
                    new Runnable() {
                        public void run() {
                            mediacontroller.show(0);
                        }},
                    100);

            if(newsReport.getNewsRating()!=null)
            {

                ratingbar.setRating(Float.parseFloat(newsReport.getNewsRating().toString()));
            }
        }
    }
}
