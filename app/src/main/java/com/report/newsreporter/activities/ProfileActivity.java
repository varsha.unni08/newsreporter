package com.report.newsreporter.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;
import com.report.newsreporter.R;
import com.report.newsreporter.connectivity.NetConnection;
import com.report.newsreporter.data.appconstants.Literals;
import com.report.newsreporter.data.pojo.RegisterData;
import com.report.newsreporter.data.pojo.ReporterProfile;
import com.report.newsreporter.preferencehelper.PreferenceHelper;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.utils.Utilities;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.RetrofitHelperLogin;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    ImageView imgprofile;
    FloatingActionButton imgedit;

    ReporterProfile reporterProfile;
    TextView txtName,txtEmail,txtdob,txtpin;
    AppCompatImageView imgback;
    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        getSupportActionBar().hide();
        imgprofile=findViewById(R.id.imgprofile);
        imgback=findViewById(R.id.imgback);
        imgedit=findViewById(R.id.imgedit);
        txtName=findViewById(R.id.txtName);
        txtEmail=findViewById(R.id.txtEmail);
        txtdob=findViewById(R.id.txtdob);
        txtpin=findViewById(R.id.txtpin);



        reporterProfile=(ReporterProfile)getIntent().getSerializableExtra("Reporterprofile");

        txtName.setText(reporterProfile.getDsReporterUser());
        txtEmail.setText(reporterProfile.getDsEmail());
        txtdob.setText(reporterProfile.getDtBirth());
        txtpin.setText(reporterProfile.getDsMobile());
        Glide.with(ProfileActivity.this)
                .load(reporterProfile.getDsEmployeeImage())
                .placeholder(R.drawable.user)

                .into(imgprofile);


        imgedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utilities.checkPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

        pickImage(11);
                } else {


                    ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 11);


                }

            }
        });

        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onBackPressed();
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 || grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                pickImage(requestCode);

        }
    }

    public void pickImage(int code) {
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, code);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {


            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            uploadImage(new File(picturePath));
            cursor.close();
        }
        }


        public void uploadImage(final File file_profile)
        {
            progressFragment = new ProgressFragment();
            progressFragment.show(getSupportFragmentManager(), "dklk");
            WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(ProfileActivity.this).create(WebInterfaceimpl.class);


            RequestBody requestFile =
                    RequestBody.create(
                            MediaType.parse(Utilities.getMimeType(file_profile)),
                            file_profile
                    );

            MultipartBody.Part body_profile =
                    MultipartBody.Part.createFormData("ds_employee_image", file_profile.getName(), requestFile);


            Call<List<RegisterData>>jsonArrayCall=webInterfaceimpl.EditProfileimg(body_profile);

            jsonArrayCall.enqueue(new Callback<List<RegisterData>>() {
                @Override
                public void onResponse(Call<List<RegisterData>> call, Response<List<RegisterData>> response) {

                    progressFragment.dismiss();

                    if(response.code()==200)
                    {

                        if(response.body().size()>0)
                        {


                            Toast.makeText(ProfileActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();


                            Glide.with(ProfileActivity.this)
                                    .load(file_profile)

                                    .into(imgprofile);


                        }
                    }
                }

                @Override
                public void onFailure(Call<List<RegisterData>> call, Throwable t) {

                    progressFragment.dismiss();
                    if(!NetConnection.isConnected(ProfileActivity.this))
                    {
                        Toast.makeText(ProfileActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                    }

                }
            });

        }
}
