package com.report.newsreporter.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.report.newsreporter.R;
import com.report.newsreporter.data.pojo.NewsCategory;
import com.report.newsreporter.data.pojo.NewsPreview;
import com.report.newsreporter.data.pojo.NewsReport;
import com.report.newsreporter.data.pojo.PinCode;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.utils.Utilities;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewReportActivity extends AppCompatActivity {

    AppCompatImageView imgback,imgDropdowncategory,imgDropdownpriority;

    TextView txtcategory,txtPriority,txtUpload,txtuploadVideo;
    ProgressFragment progressFragment;

    NewsCategory newsCategory;
    Button btnUploadimage,btnuploadvideo;

    String priority="";

    int image=100,video=102;

    ImageView imgView,imgupload,imguploadvideo;

    LinearLayout layoutImgupload,layoutVideorecordbtn;
    VideoView videoview;

    File imgfile=null,videofile=null;
    Button btnSubmit;
    EditText edtTitle,edtContent;

    Uri contentURI=null;
    NestedScrollView scroll;

    ImageButton imgBtnAddImg,imgBtnAddVideo;

    AppCompatImageView imgrecordaudio;

    RelativeLayout relvideo,relimg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_report);
        getSupportActionBar().hide();
        scroll=findViewById(R.id.scroll);
        imgback=findViewById(R.id.imgback);
        imgDropdowncategory=findViewById(R.id.imgDropdowncategory);
        txtcategory=findViewById(R.id.txtcategory);
        txtUpload=findViewById(R.id.txtUpload);
        txtPriority=findViewById(R.id.txtPriority);
        imgDropdownpriority=findViewById(R.id.imgDropdownpriority);
        btnUploadimage=findViewById(R.id.btnUploadimage);
        btnSubmit=findViewById(R.id.btnSubmit);
        imgView=findViewById(R.id.imgView);
        imgupload=findViewById(R.id.imgupload);
        layoutImgupload=findViewById(R.id.layoutImgupload);
        layoutVideorecordbtn=findViewById(R.id.layoutVideorecordbtn);

        relvideo=findViewById(R.id.relvideo);

        relimg=findViewById(R.id.relimg);

        edtContent=findViewById(R.id.edtContent);
        edtTitle=findViewById(R.id.edtTitle);

        btnuploadvideo=findViewById(R.id.btnuploadvideo);

        txtuploadVideo=findViewById(R.id.txtuploadVideo);
        imguploadvideo=findViewById(R.id.imguploadvideo);
        videoview=findViewById(R.id.videoview);


        imgBtnAddImg=findViewById(R.id.imgBtnAddImg);
        imgBtnAddVideo=findViewById(R.id.imgBtnAddVideo);
        imgrecordaudio=findViewById(R.id.imgrecordaudio);

//        edtTitle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                edtTitle.requestFocus();
//                edtTitle.setFocusable(true);
//                edtTitle.setFocusableInTouchMode(true);
//
//            }
//        });


        imgBtnAddImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickImage(image);
                } else {

                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
                }
            }
        });

        imgBtnAddVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

                    pickVideo();
                } else {

                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
                }
            }
        });




        scroll.post(new Runnable() {
            @Override
            public void run() {
                scroll.fullScroll(View.FOCUS_UP);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtTitle.getText().toString().equals(""))
                {

                    if(newsCategory!=null) {

                        if (!edtContent.getText().toString().equals("")) {

                            NewsPreview newsPreview=new NewsPreview();
                            newsPreview.setCategory(newsCategory);
                            newsPreview.setContent(edtContent.getText().toString());
                            newsPreview.setTitle(edtTitle.getText().toString());
                            newsPreview.setPriority(priority);
                            if(videofile!=null) {
                                newsPreview.setVideopath(videofile.getAbsolutePath());
                                newsPreview.setVideouri(contentURI.toString());
                            }
                            if(imgfile!=null) {
                                newsPreview.setImagepath(imgfile.getAbsolutePath());

                            }



                            startActivity(new Intent(AddNewReportActivity.this, NewspreviewActivity.class).putExtra("newspreview",newsPreview));




                        } else {

                            Toast.makeText(AddNewReportActivity.this, "Enter content", Toast.LENGTH_SHORT).show();
                        }


                    }
                    else {


                        Toast.makeText(AddNewReportActivity.this, "Select news category", Toast.LENGTH_SHORT).show();

                    }




                }
                else {

                    Toast.makeText(AddNewReportActivity.this,"Enter title",Toast.LENGTH_SHORT).show();
                }
            }
        });






//        btnuploadvideo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                   pickVideo();
//                } else {
//
//                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
//                }
//
//            }
//        });
//
//        txtuploadVideo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                    pickVideo();
//                } else {
//
//                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
//                }
//
//            }
//        });
//
//        imguploadvideo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                    pickVideo();
//                } else {
//
//                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
//                }
//
//            }
//        });
//
//
//        btnUploadimage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                    pickImage(image);
//                } else {
//
//                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
//                }
//
//
//            }
//        });
//
//        txtUpload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                    pickImage(image);
//                } else {
//
//                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
//                }
//
//            }
//        });
//
//        btnUploadimage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (Utilities.checkPermission(AddNewReportActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
//
//                    pickImage(image);
//                } else {
//
//                    ActivityCompat.requestPermissions(AddNewReportActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, image);
//                }
//
//
//            }
//        });







        txtPriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPriority();

            }
        });

        imgDropdownpriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPriority();

            }
        });




        imgback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                onBackPressed();
            }
        });

        txtcategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showCategory();

            }
        });

        imgDropdowncategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showCategory();
            }
        });


    }

    public void showCategory()
    {
        progressFragment=new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(),"kdfmli");

        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(AddNewReportActivity.this).create(WebInterfaceimpl.class);

        Call<List<NewsCategory>>jsonArrayCall=webInterfaceimpl.getNewsCategory();
        jsonArrayCall.enqueue(new Callback<List<NewsCategory>>() {
            @Override
            public void onResponse(Call<List<NewsCategory>> call, Response<List<NewsCategory>> response) {
                progressFragment.dismiss();

                //Log.e("category",response.body().toString());
                if(response.body()!=null)
                {
                    if(response.code()==200) {


                        showCategoryAlert(response.body());
                    }




                }




            }

            @Override
            public void onFailure(Call<List<NewsCategory>> call, Throwable t) {
                progressFragment.dismiss();

            }
        });
    }


    public void showCategoryAlert(final List<NewsCategory>dist)
    {

        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewReportActivity.this);
        builder.setTitle("Choose a news category");
        for (NewsCategory st:dist)
        {
            strings.add(st.getDsNewsCategory());
        }


        builder.setItems(strings.toArray(new String[strings.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
               // pinCode=dist.get(which);
                newsCategory=dist.get(which);

                txtcategory.setText(newsCategory.getDsNewsCategory());

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showPriority()
    {
        List<String>strings=new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNewReportActivity.this);
        builder.setTitle("Choose a priority");
       final String arr[]={"National","state","district","None"};


        builder.setItems(arr, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                // pinCode=dist.get(which);
                priority=arr[which];

               txtPriority.setText(priority);

            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();

    }



    public void pickImage(int code) {
        Intent i = new Intent(
                Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(i, code);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && null != data) {

            if (requestCode == image) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String picturePath = cursor.getString(columnIndex);

                File file_profile = new File(picturePath);
                Glide.with(AddNewReportActivity.this)
                        .load(file_profile)

                        .into(imgView);
                cursor.close();

                imgfile=file_profile;
                relimg.setVisibility(View.VISIBLE);

//                layoutImgupload.setVisibility(View.VISIBLE);
//                btnUploadimage.setVisibility(View.GONE);

            }

            else if(requestCode==video)
            {
                 contentURI = data.getData();

                String selectedVideoPath = getPath(contentURI);
                Log.d("path",selectedVideoPath);
              //  saveVideoToInternalStorage(selectedVideoPath);

                final MediaController mediacontroller = new MediaController(AddNewReportActivity.this);
                mediacontroller.setAnchorView(videoview);

                videoview.setVideoURI(contentURI);
                videoview.setMediaController(mediacontroller);
                videoview.requestFocus();
                videoview.start();
                videofile=new File(selectedVideoPath);
                relvideo.setVisibility(View.VISIBLE);

//                btnuploadvideo.setVisibility(View.GONE);
//                layoutVideorecordbtn.setVisibility(View.VISIBLE);
            }
        }
    }

    public void pickVideo()
    {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, video);
    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            // HERE YOU WILL GET A NULLPOINTER IF CURSOR IS NULL
            // THIS CAN BE, IF YOU USED OI FILE MANAGER FOR PICKING THE MEDIA
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }
}
