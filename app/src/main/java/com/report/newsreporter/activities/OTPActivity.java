package com.report.newsreporter.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonArray;
import com.report.newsreporter.R;
import com.report.newsreporter.connectivity.NetConnection;
import com.report.newsreporter.data.appconstants.Literals;
import com.report.newsreporter.data.pojo.OtpGenerateData;
import com.report.newsreporter.preferencehelper.PreferenceHelper;
import com.report.newsreporter.progress.ProgressFragment;
import com.report.newsreporter.webservice.RetrofitHelper;
import com.report.newsreporter.webservice.WebInterfaceimpl;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OTPActivity extends AppCompatActivity {

    String Email="";
    TextView txtotpmsg;
    EditText edtCode;
    FloatingActionButton fab;
    ProgressFragment progressFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        getSupportActionBar().hide();

        txtotpmsg=findViewById(R.id.txtotpmsg);
        edtCode=findViewById(R.id.edtCode);
        fab=findViewById(R.id.fab);

        Intent intent=getIntent();
        Email=intent.getStringExtra("Email");

        txtotpmsg.setText("Please type otp verification code that sent to "+Email);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!edtCode.getText().toString().equals(""))
                {


                    verifyOTP();
                }
                else {

                    Toast.makeText(OTPActivity.this,"Enter Otp code",Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    public void verifyOTP()
    {


        progressFragment = new ProgressFragment();
        progressFragment.show(getSupportFragmentManager(), "dklk");
        WebInterfaceimpl webInterfaceimpl = RetrofitHelper.getRetrofitInstance(OTPActivity.this).create(WebInterfaceimpl.class);

        Call<List<OtpGenerateData>> jsonArrayCall=webInterfaceimpl.otpVerification(edtCode.getText().toString().trim());
        jsonArrayCall.enqueue(new Callback<List<OtpGenerateData>>() {
            @Override
            public void onResponse(Call<List<OtpGenerateData>> call, Response<List<OtpGenerateData>> response) {

                progressFragment.dismiss();

                Log.e("OTPVerifydata",response.body().toString());

              //  [{"status":1,"Message":"Registration complete."}]

                if(response.body()!=null)
                {
                    if(response.code()==200)
                    {

                        //Log.e("Otpgenerate",response.body().toString());

                        if(response.body().size()>0)
                        {

                            if(response.body().get(0).getStatus()==1)
                            {


                                Toast.makeText(OTPActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();


                                new PreferenceHelper(OTPActivity.this).putBooleanData(Literals.isRegistered,true);
                               // startActivity(new Intent(OTPActivity.this,OTPActivity.class));




                                AlertDialog.Builder builder=new AlertDialog.Builder(OTPActivity.this);
                                builder.setTitle(R.string.app_name);
                                builder.setCancelable(false);
                                builder.setMessage("Please contact your admin for verification ");
                                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {


                                        Intent intent=new Intent(OTPActivity.this,LoginActivity.class);

                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                });

                                builder.show();

                            }
                            else {

                                Toast.makeText(OTPActivity.this,response.body().get(0).getMessage(),Toast.LENGTH_SHORT).show();

                            }
                        }



                    }
                }

            }

            @Override
            public void onFailure(Call<List<OtpGenerateData>> call, Throwable t) {
                progressFragment.dismiss();
                if(!NetConnection.isConnected(OTPActivity.this))
                {
                    Toast.makeText(OTPActivity.this,"Check Internet connection",Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
